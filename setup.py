#!/usr/bin/env python
# vim: set sts=4 ts=4 sw=4 filetype=python expandtab:
import os
import setuptools

script_dirname = os.path.dirname( __file__ )
script_dirname_abs = os.path.abspath( script_dirname )
script_basename = os.path.basename( __file__ )

with open( os.path.join( script_dirname, "DESCRIPTION.rst" ), "r" ) as file_stream:
    long_description = file_stream.read()

module_name = "arcyvtool"

setuptools.setup(
    name = module_name,

#    version = "1.0.9999",

    author = "Iwan Aucamp",
    author_email = "aucampia@gmail.com",

    maintainer = "Iwan Aucamp",
    maintainer_email = "aucampia@gmail.com",

    url = "https://example.com/something/",

    description = "TODO",

    long_description = long_description,

    download_url = "https://example.com/something/somefile",

    classifiers=[
            "Development Status :: 3 - Alpha",
    ],

#   platforms = [ "", "", "", ]

    license = "TODO FIXME",

    keywords="key word",

    package_dir = { "" : "src" },

#   packages = setuptools.find_packages( where=".", exclude=[], include=[ "*" ] ),
    packages = [
        module_name,
    ],

    entry_points = {
        "console_scripts": [
            module_name + " = " + module_name + ".cli:main",
        ]
    },

    setup_requires=[ "vcversioner" ],

    vcversioner = {
        "version_file" : "version.txt",
        "version_module_paths" : [ "src/" + module_name + "/__version__.py" ]
    },

)
