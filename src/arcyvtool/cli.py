#!/usr/bin/env python
# vim: set sts=4 ts=4 sw=4 filetype=python expandtab:
import sys
import fnmatch
import os
import argparse
import zipfile
import json
import ntpath

import rarfile
#import path.py

import pyreevu

import arcyvtool

import shutil

class ArchiveTree( object ):

    def __init__( self, path_module ):
        self.path_module = path_module
        self.tree = {}

    def extend( self, path_strings ):
        for path_string in path_strings:
            path_components = self.path_module.normpath( path_string ).split( self.path_module.sep )
            verbose.write( 3, "path_components = {}".format( json.dumps( path_components ) ) )
            self.append( path_components )

    def append( self, components ):
        current = self.tree
        for component in components:
            if component not in current:
                current[ component ] = {}
            current = current[ component ]

class FileExistsError( Exception ):
    def __init__( self, value ):
        self.value = value
    def __str__( self ):
        return repr( self.value )

class Cli( object ):

    def __init__( self ):
        global verbose
        verbose = pyreevu.Verbose( None )

    def brute_force_rar_file( self, some_file ):
        for password in self.password_list:
            try:
                verbose.write( 3, "trying password = '{}'".format( password ) )
                some_file.setpassword( password )
                some_file.testrar()
                verbose.write( 1, "password = '{}' OK".format( password ) )
                return password
            except rarfile.RarNoFilesError as exception:
                verbose.write( 3, "exception = {}".format( exception ) )
            except rarfile.BadRarFile as exception:
                verbose.write( 3, "exception = {}".format( exception ) )
            except rarfile.RarCRCError as exception:
                verbose.write( 3, "exception = {}".format( exception ) )
        raise RuntimeError( "Could not determine password for {}".format( rar_file.volumelist() ) )

    def main( self ):

        version_string = "%(prog)s {}-{}".format( arcyvtool.__version__, arcyvtool.__revision__ )
        argument_parser = argparse.ArgumentParser( prog=os.path.basename( sys.argv[ 0 ] ), description = version_string, add_help = False )
        argument_parser.add_argument( "-v", "--verbose", action="count", dest="verbosity", help="increase verbosity level" )
        argument_parser.add_argument( "-h", "--help", action="help", help="increase verbosity level" )
        argument_parser.add_argument( "--version", action="version", version=( version_string ) )
        argument_parser.add_argument( "--passwords", action="store", dest="password_list_file_name", type=str, default=None, help="increase verbosity level" )

        argument_parser.add_argument( "--destination", action="store", dest="destination", type=str, default=None, help="set destination folder" )
        argument_parser.add_argument( "--error-dir", action="store", dest="error_dir", type=str, default=None, required=True, help="set error directory" )
        argument_parser.add_argument( "--trash", action="store", dest="trash", type=str, default=None, required=True, help="..." )
        argument_parser.add_argument( "--dry-run", action="store_true", dest="dry_run", default=False, help="don't do anything" )
        argument_parser.add_argument( "--subdir-threshold", action="store", dest="subdir_threshold", type=int, default=5, help="..." )

        argument_parser.add_argument( "--max-depth", action="store", dest="max_depth", type=int, default=0, help="..." )

        argument_parser.add_argument( "directories", nargs="+", type=str, help="directories" )

        self.arguments = argument_parser.parse_args( args = sys.argv[1:] )

        verbose.level = self.arguments.verbosity
        verbose.write( 3, "pyreevu.__version__ = {}".format( pyreevu.__version__ ) )
        verbose.write( 3, "pyreevu.__revision__ = {}".format( pyreevu.__revision__ ) )
        verbose.write( 3, "arcyvtool.__version__ = {}".format( arcyvtool.__version__ ) )
        verbose.write( 3, "arcyvtool.__revision__ = {}".format( arcyvtool.__revision__ ) )

        verbose.write( 1, "arguments.directories = {}".format( self.arguments.directories ) )
        verbose.write( 1, "arguments.verbosity = {}".format( self.arguments.verbosity ) )

        verbose.write( 0, "arguments.dry_run = {}".format( self.arguments.dry_run ) )
        verbose.write( 0, "arguments.destination = {}".format( self.arguments.destination ) )
        verbose.write( 0, "arguments.subdir_threshold = {}".format( self.arguments.subdir_threshold ) )
        verbose.write( 0, "arguments.max_depth = {}".format( self.arguments.max_depth ) )
        verbose.write( 0, "arguments.trash = {}".format( self.arguments.trash ) )


        if not os.path.isdir( self.arguments.trash ):
            msg = "trash {} does not exist".format( self.arguments.trash )
            sys.stderr.write( msg + "\n" )
            raise RuntimeError( msg )

        if self.arguments.password_list_file_name is not None:
            with open( self.arguments.password_list_file_name ) as stream:
                self.password_list = [ line.rstrip( "\r\n" ) for line in stream ]
            verbose.write( 1, "self.password_list = {}".format( self.password_list ) )

        rar_file_names = set()
        zip_file_names = set()

        for directory in self.arguments.directories:
            directory_abs = os.path.join( os.path.abspath( directory ), "" )
            directory_abs_components = directory_abs.split( os.path.sep )
            directory_abs_components_len = len( directory_abs_components )
            for root, dirnames, filenames in os.walk( directory ):
                root_abs = os.path.join( os.path.abspath( root ), "" )
                root_abs_components = root_abs.split( os.path.sep )
                root_abs_components_len = len( root_abs_components )
                depth = root_abs_components_len - directory_abs_components_len
                verbose.write( 1, "dir = ( {}, {}, {} ), root = ( {}, {}, {} ), depth = {}"
                    .format(
                        directory, directory_abs_components, directory_abs_components_len,
                        root, root_abs_components, root_abs_components_len,
                        depth ) )
                if depth > self.arguments.max_depth: continue
                for rar_file_name in fnmatch.filter( filenames, "*.rar" ):
                    rar_file_names.add( os.path.abspath( os.path.join( root, rar_file_name ) ) )
                for zip_file_name in fnmatch.filter( filenames, "*.zip" ):
                    zip_file_names.add( os.path.abspath( os.path.join( root, zip_file_name ) ) )

        rar_file_names = sorted( rar_file_names )
        zip_file_names = sorted( zip_file_names )

        verbose.write( 2, "zip_file_names = {}".format( json.dumps( zip_file_names, indent=4 ) ) )
        verbose.write( 2, "rar_file_names = {}".format( json.dumps( rar_file_names, indent=4 ) ) )

        for rar_file_name in rar_file_names:
            rar_file = None
            rar_file_volumelist = []
            try:
                rar_file_name_dir = os.path.dirname( rar_file_name )
                rar_file_name_base = os.path.basename( rar_file_name )
                rar_file_name_dest = os.path.splitext( rar_file_name_base )[ 0 ]
                verbose.write( 1, "rar_file_name( . = '{}', dir = '{}', base = '{}', dest = '{}' )"
                    .format( rar_file_name, rar_file_name_dir, rar_file_name_base, rar_file_name_dest ) )
                if not os.path.exists( rar_file_name ): continue
                rar_file = rarfile.RarFile( rar_file_name )
                verbose.write( 3, "rar_file = {}".format( rar_file ) )
                rar_file_needs_password = rar_file.needs_password()
                rar_file_password = None
                if rar_file_needs_password:
                    rar_file_password = self.brute_force_rar_file( rar_file )
                    rar_file.close();
                    rar_file = rarfile.RarFile( rar_file_name )
                    rar_file.setpassword( rar_file_password )
                else:
                    rar_file.testrar()
                verbose.write( 2, "rar_file.needs_password() = {}".format( rar_file_needs_password ) )
                rar_file_volumelist = rar_file.volumelist()
                verbose.write( 1, "rar_file.volumelist() = {}".format( json.dumps( rar_file_volumelist ) ) )
                rar_file_namelist = rar_file.namelist()
                verbose.write( 3, "rar_file.namelist() = {}".format( json.dumps( rar_file_namelist ) ) )
                tree = ArchiveTree( path_module = ntpath )
                tree.extend( rar_file_namelist )
                verbose.write( 2, "tree.tree = {}".format( json.dumps( tree.tree, indent=4 ) ) )
                if self.arguments.destination is not None:
                    destination = self.arguments.destination
                else:
                    destination = rar_file_name_dir
                dest_exists = None
                direct_files = list( tree.tree.keys() )
                verbose.write( 1, "len( direct_files ) = {}".format( json.dumps( len( direct_files ) ) ) )
                if ( len( direct_files ) >= self.arguments.subdir_threshold ):
                    destination = os.path.join( destination, rar_file_name_dest )
                    if ( os.path.exists( destination ) ): dest_exists = destination
                else:
                    for direct_file in direct_files:
                        direct_file_abs = os.path.join( destination, direct_file )
                        verbose.write( 1, "direct_file_abs = {}".format( json.dumps( direct_file_abs ) ) )
                        if os.path.exists( direct_file_abs ):
                            dest_exists = direct_file_abs
                            break
                if dest_exists is not None:
                    msg = "archive {} : file {} exists: skip archive".format( rar_file_name, dest_exists )
                    sys.stderr.write( msg + "\n" )
                    raise FileExistsError( msg )
                    continue
                verbose.write( 1, "destination = {}".format( json.dumps( destination ) ) )

                msg = "archive {} : extracting to destination '{}' with password '{}'".format( rar_file_name, destination, rar_file_password )
                sys.stderr.write( msg + "\n" )

                if not self.arguments.dry_run:
                    rar_file.extractall( path = destination, pwd = rar_file_password )
                    rar_file.close()
                    rar_file = None
                    for rar_file_volume in rar_file_volumelist:
                        shutil.move( rar_file_volume, self.arguments.trash )


            except rarfile.NeedFirstVolume as exception:
                verbose.write( 2, "exception = {}".format( exception ) )
            except FileExistsError as exception:
                verbose.write( 2, "exception = {}".format( exception ) )
                sys.stderr.write( "archive {} : moving '{}' to '{}'".format( rar_file_name, rar_file_volumelist, self.arguments.error_dir ) );
                if not self.arguments.dry_run:
                    for rar_file_volume in rar_file_volumelist:
                        shutil.move( rar_file_volume, self.arguments.error_dir )
            finally:
                if rar_file is not None: rar_file.close()




def main():
    cli = Cli();
    cli.main();

if __name__ == "__main__":
    main()
